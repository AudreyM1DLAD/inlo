# encoding : utf8

# Pour le 25 Janvier 2023 : Faire une classe Animal qui possède
# plusieurs attributs et méthodes. Dans ces méthodes, on doit pouvoir
# créer des enfants. Savoir quels sont les parents des enfants. Savoir
# quels individus ont des enfants etc. 


# %%

# Definition de la classe Animal
class Animal():
    
    def __init__(self = 0, species = 0, age = 0, diet = 0, sex = 0):
        self.species = species
        self.age = age
        self.diet = diet
        self.sex = sex
        self.childrens = []

# Ajouter ou enlever des enfants
    def add_childrens(self,child):
        self.childrens.append(child)

    def remove_childrens(self,child):
        self.childrens.remove(child)

# Trouver les parents d'un enfant  (NON FONCTIONNEL)
    def find_parents_of_child(self,a):
        i = 0
        while i < len(self.childrens):
            if self.childrens[i] == "a" and self.sex == "male":
                print(self, "is the father of", a)
                i += 1
            elif self.childrens[i] == "a" and self.sex == "female":
                print(self, "is the mother of", a)
                i += 1
            else : 
                print("This child isn't recorded")
                break

    def findyou(cls):
        print("Les enfants répertoriés sont :", cls.childrens)
    
    
    
# Savoir si un individu a des enfants ok
    def is_parent(self):
        if not(self.childrens):
            print("This animal has no child")
        else : 
            print("This animal has one child or more")
            print("Here they are : ", self.childrens)


# Ajout d'animaux dans la classe 
animal1 = Animal("snake", 13, "carnivore")
animal2 = Animal("snake", 12, "carnivore")
animal3 = Animal("chicken", 4, "omnivore")
animal4 = Animal("pig", 7, "omnivore")
animal5 = Animal("pig", 6,"omnivore")

# Ajout d'enfants à certains animaux ok
animal1.add_childrens("animal1_1")
animal1.add_childrens("animal1_2")
animal4.add_childrens("animal4_1")
animal4.add_childrens("animal4_2")
animal4.add_childrens("animal4_3")


# Petits tests
print(animal1.childrens)

animal1.is_parent()
print("___")
animal2.is_parent()
print("___")

print(len(animal1.childrens))
print(animal1.childrens[1])
print("___")

# %%
