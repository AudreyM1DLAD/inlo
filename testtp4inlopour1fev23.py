# utf-8 encoding

# TP 4 INLO - Pour le 1er Février 2023

class Binary_tree():
    def __init__(self):
        self.root=None

    def print_tree(self):
        return self.root.display_node()
        

class Node():
# pour initialiser l'arbre
    def __init__(self, value):
        self.value=value
        self.right= None
        self.left= None
        self.depth= 0

# pour le continuer à partir des noeauds préexistants.
    def add(self, left = None, right = None):
        self.left = left
        self.right = right
        if self.left:
            left.depth = self.depth + 1
            left.update_children_depth()
        if self.right:
            right.depth = self.depth + 1
            right.update_children_depth()

    def update_children_depth(self): 
        if self.left:
            self.left.depth = self.depth + 1
            self.left.update_children_depth()
        if self.right:
            self.right.depth = self.depth + 1
            self.right.update_children_depth()

    def display_node(self):
        retour = str(self)
        if self.right:
            retour += " " + self.right.display_node() 
        if self.left:
            retour += " " + self.left.display_node()

        return retour

    def __str__(self):
        tree_indentation = self.value * "    "
        return tree_indentation + str(self.value) + "/" + str(self.depth) + "\n"

    def is_leaf(self):
        #return self.left == None and self.right == None
        return not(self.left or self.right)

    def get_max_depth(self,max_depth=0):
        if self.is_leaf():
            if self.depth > max_depth:
                return self.depth
            else:
                return max_depth
        # je suis le node d'un arbre
        else:
            if self.right:
                max_depth = self.right.get_max_depth(max_depth)

            if self.left:
                max_depth = self.left.get_max_depth(max_depth)
            return max_depth
    


node1 = Node(0)
node2 = Node(2)
node3 = Node(3)
node4 = Node(4)
node3.add(node4)
node1.add(node2, node3)
tree1=Binary_tree()
tree1.root=node1

node5 = Node(5)
node6 = Node(6)
node7 = Node(7)
node5.add(node6, node7)
node4.add(node5)
node8 = Node(8)
node7.add(node8)

print(str(node1.get_max_depth(0)))


# print(node1.display_node())

print("Affichage en profondeur d'un arbre binaire (vue horizontale): ")
print(tree1.print_tree())

# J'observe un léger décalage entre 3/1 et 2/1 par exemple et je ne comprends pas pourquoi.